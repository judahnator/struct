<?php

namespace judahnator\Struct\Tests;

use judahnator\Struct;
use PHPUnit\Framework\TestCase;
use RuntimeException;

final class StructTest extends TestCase
{
    public function testBasicStructUsage(): void
    {
        $structure = new class extends Struct {
            /**
             * @var string
             */
            protected $foo = 'Hello World';

            /**
             * @var int
             */
            protected $bar = 1;

            /**
             * @var \stdClass
             */
            protected $bing;

            /**
             * @var \stdClass
             */
            protected $baz;

            /**
             * @var Struct
             */
            protected $bong;
        };

        $inheritedStdClass = new class extends \stdClass {
        };
        $inheritedStructClass = new class extends Struct {
        };

        $structure->foo = 'Hello World!';
        $structure->bar = 2;
        $structure->bing = new \stdClass();
        $structure->baz = $inheritedStdClass;
        $structure->bong = $inheritedStructClass;

        $this->assertEquals('Hello World!', $structure->foo);
        $this->assertEquals(2, $structure->bar);
        $this->assertEquals(new \stdClass(), $structure->bing);
        $this->assertEquals($inheritedStdClass, $structure->baz);
        $this->assertEquals($inheritedStructClass, $structure->bong);
    }

    public function testInvalidArgumentException(): void
    {
        $this->expectException(\InvalidArgumentException::class);
        $this->expectExceptionMessage('anInt must be an instance of int');
        $structure = new class extends Struct {
            /**
             * @var int
             */
            protected $anInt = 0;
        };

        $structure->anInt = 'this will fail';
    }

    public function testInvalidPropertyType(): void
    {
        $this->expectException(RuntimeException::class);
        $this->expectExceptionMessage('Could not resolve property type for foobar');
        $struct = new class extends Struct {
            protected $foobar;
        };
        $struct->foobar = 'bingbaz';
    }
}
