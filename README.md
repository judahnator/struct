PHP Struct
==========

[![pipeline status](https://gitlab.com/judahnator/struct/badges/master/pipeline.svg)](https://gitlab.com/judahnator/struct/commits/master)

Allows you to define data structures in PHP. Thats kinda it. Go home.

Installation
============

Use composer. You know how.

Usage
=====

Make your class extend the `judahnator\Struct` class, then define properties with the appropriate phpdoc comments.

For example:

```php
<?php

use judahnator\Struct;

final class Person extends Struct
{
    /**
     * @var string
     */
    protected $name;
        
    /**
     * @var int 
     */
    protected $age;
    
    /**
     * @var boolean
     */
    protected $alive;
    
    /**
     * @var Person
     */
    protected $bestFriend;
}

$person = new Person();
$person->name = 'Bob';
$person->age = 42;
$person->alive = true;
$person->bestFriend = new Hat(); // will throw exception, you cant be friends with a hat
```