<?php

namespace judahnator;

abstract class Struct
{
    public function __get($name)
    {
        return $this->{$name};
    }

    public function __set($name, $value)
    {
        $type = $this->getPropertyType($name);

        if (!self::isValidType($value, $type)) {
            throw new \InvalidArgumentException("$name must be an instance of {$type}");
        }

        $this->{$name} = $value;
    }

    private function getPropertyType(string $property): string
    {
        // Set up reflection, we will need it to see into the property
        $reflection = new \ReflectionClass($this);

        // Attempt to resolve the variable from the @var phpdoc statement
        if (
            !preg_match(
                '/@var\s(.+)/m',
                $reflection->getProperty($property)->getDocComment() ?: '',
                $phpDoc
            )
        ) {
            throw new \RuntimeException('Could not resolve property type for ' . $property);
        }

        if (
            // If it is not a generic type
            !in_array($phpDoc[1], ['array', 'bool', 'boolean', 'int', 'integer', 'object', 'string']) &&

            // And the class does not exist
            !class_exists($phpDoc[1]) &&

            // But it is referenced in the use statements...
            preg_match(
                '/^use\s([\w\\\]+\\\\' . $phpDoc[1] . ');$/m',
                file_get_contents($reflection->getFileName() ?: __FILE__) ?: '',
                $namespace
            )
        ) {
            // Use the namespaced class
            return $namespace[1];
        }

        // fallback to the phpdoc declaration if we cant find anything
        return $phpDoc[1];
    }

    private static function isValidType($input, string $type): bool
    {
        switch ($type) {

            case 'array':
                return is_array($input);

            case 'bool':
            case 'boolean':
                return is_bool($input);

            case 'int':
            case 'integer':
                return is_int($input);

            case 'mixed':
                return true;

            case 'object':
                return is_object($input);

            case 'string':
                return is_string($input);

            case class_exists($type):
                return $input instanceof $type;

            default:
                return false;
        }
    }
}
